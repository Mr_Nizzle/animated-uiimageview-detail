//
//  FOTMainViewController.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/7/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTMainViewController.h"
#import <objc/runtime.h> // for objc_setAssociatedObject / objc_getAssociatedObject


@interface FOTMainViewController ()

@property BOOL isOpen;
@property UIImageView *ivExpand;

@end

@implementation FOTMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Basic";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(singleTapGestureCaptured:)];
    [_imageView addGestureRecognizer:singleTap];
    [_imageView setMultipleTouchEnabled:YES];
    [_imageView setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)singleTapGestureCaptured:(id)sender{
    if (_isOpen) {
        [UIView transitionWithView: self.view
                          duration: 1.0
                           options: UIViewAnimationOptionAllowAnimatedContent
                        animations:^{
                            _ivExpand.frame = self.imageView.frame;
                            
                        } completion:^(BOOL finished) {
                            _isOpen = NO;
                            [_ivExpand removeFromSuperview];
                        }];
    }
    else{
        _ivExpand = [[UIImageView alloc] initWithImage: _imageView.image];
        _ivExpand.contentMode = _imageView.contentMode;
        _ivExpand.frame = [self.view convertRect: _imageView.frame fromView: _imageView.superview];
        _ivExpand.userInteractionEnabled = YES;
        _ivExpand.clipsToBounds = YES;
        _ivExpand.backgroundColor = [UIColor whiteColor];
        
        [UIView transitionWithView: self.view
                          duration: 1.0
                           options: UIViewAnimationOptionAllowAnimatedContent
                        animations:^{
                            
                            [self.view addSubview: _ivExpand];
                            _ivExpand.frame = self.view.bounds;
                            
                        } completion:^(BOOL finished) {
                            _isOpen = YES;
                            UITapGestureRecognizer* tgr = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector( singleTapGestureCaptured: )];
                            [_ivExpand addGestureRecognizer: tgr];
                        }];
    }
}
@end
