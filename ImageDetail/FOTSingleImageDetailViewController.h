//
//  FOTSingleImageDetailViewController.h
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SingleImageDetailDelegate <NSObject>

@required

-(void)shouldDismissImageDetail;

@end

@interface FOTSingleImageDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage *theImage;
@property (nonatomic, strong) UIImageView *imageView;
@property CGRect originalFrame;
- (IBAction)dismissImageDetail:(id)sender;

@property (nonatomic, assign) id<SingleImageDetailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *topMenuView;
@end
