//
//  FOTThirdViewController.h
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FOTThirdViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@end
