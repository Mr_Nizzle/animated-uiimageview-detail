//
//  FOTReelDetailViewController.h
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReelDetailDelegate <NSObject>

@required

-(void)shouldDismissReelDetail;

@optional

-(void)reelPageChanged:(int)page;

@end

@interface FOTReelDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSArray *imagesArr;
@property int initPage;

-(void)createPages;

@property (weak, nonatomic) IBOutlet UIView *topMenuView;
- (IBAction)dismissDetailView:(id)sender;

@property (nonatomic, assign) id<ReelDetailDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *scrollsList;
@end
