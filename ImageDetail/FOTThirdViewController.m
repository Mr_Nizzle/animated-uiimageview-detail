//
//  FOTThirdViewController.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTThirdViewController.h"
#import "FOTReelDetailViewController.h"

@interface FOTThirdViewController () <UIScrollViewDelegate, ReelDetailDelegate>
@property (nonatomic, strong) NSArray *imagesArr;
@property int currentPage;
@property (nonatomic, strong) FOTReelDetailViewController *reelViewcontroller;
@property BOOL isShowingDetail;
@property (nonatomic, strong) NSMutableArray *scrollsList;
@end

@implementation FOTThirdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Reel";
        _imagesArr = @[
                       @"01.jpg",
                       @"02.jpg",
                       @"03.jpg",
                       @"04.jpg",
                       @"05.jpg",
                       @"06.jpg"
                       ];
        _scrollsList = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _mainScrollView.delegate = self;
    [self createPages];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createPages{
    for (int i = 0; i < _imagesArr.count; i++) {
        // calc frame
        CGRect frame;
        frame.origin.x = (self.mainScrollView.frame.size.width - 0) * i;
        frame.origin.y = 0;
        frame.size = self.mainScrollView.frame.size;
        
        // create subview
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        [subview setBackgroundColor:[UIColor clearColor]];
        
        // subview image
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 240, 240)];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
        [imageView setBackgroundColor:[UIColor blackColor]];
        [imageView setImage:[UIImage imageNamed:[_imagesArr objectAtIndex:i]]];
        
        // Gesture Recognizer
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(singleTapGestureCaptured:)];
        [imageView addGestureRecognizer:singleTap];
        [imageView setMultipleTouchEnabled:YES];
        [imageView setUserInteractionEnabled:YES];

        [_scrollsList addObject:subview];
        [subview addSubview:imageView];
        
        //add subview
        [self.mainScrollView addSubview:subview];
    }
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width * _imagesArr.count, self.mainScrollView.frame.size.height);
}

#pragma mark -
#pragma mark ScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.mainScrollView.frame.size.width;
    _currentPage = floor((self.mainScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

#pragma mark -
#pragma mark Reel Delegate Methods

-(void)singleTapGestureCaptured:(id)sender{
    UITapGestureRecognizer *theSender = (UITapGestureRecognizer *)sender;
    UIImageView *senderIV = (UIImageView *)[theSender view];
    _reelViewcontroller = [[FOTReelDetailViewController alloc] initWithNibName:@"FOTReelDetailViewController" bundle:[NSBundle mainBundle]];
    _reelViewcontroller.imagesArr = _imagesArr;
    _reelViewcontroller.initPage = _currentPage;
    _reelViewcontroller.delegate = self;
    [_reelViewcontroller.view setBackgroundColor:[UIColor clearColor]];
    [_reelViewcontroller.scrollView setBackgroundColor:[UIColor clearColor]];
    [_reelViewcontroller createPages];
    [self.tabBarController.view addSubview:_reelViewcontroller.view];
    [senderIV setAlpha:0.0f];
    _isShowingDetail = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIScrollView *currentSV = (UIScrollView *)[_reelViewcontroller.scrollsList objectAtIndex:_reelViewcontroller.initPage];
    UIImageView *curentIV = (UIImageView *)[[currentSV subviews] objectAtIndex:0];
    CGRect currentIVFrame = curentIV.frame;
    curentIV.frame = [self.view convertRect: senderIV.frame fromView: senderIV.superview];
    
    
    [UIView transitionWithView: self.view
                      duration: 0.70
                       options: UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        
                        curentIV.frame = currentIVFrame;
                        [_reelViewcontroller.scrollView setBackgroundColor:[UIColor blackColor]];
                        
                    } completion:^(BOOL finished) {
                        // do nothing
                    }];
}

-(void)shouldDismissReelDetail{
    // on main view
    UIScrollView *currentSV = (UIScrollView *)[_scrollsList objectAtIndex:_currentPage];
    UIImageView *curentIV = (UIImageView *)[[currentSV subviews] objectAtIndex:0];
    CGRect currentIVFrame = [self.view convertRect: curentIV.frame fromView: curentIV.superview];
    
    // on detail view
    UIScrollView *currentDetailSV = (UIScrollView *)[_reelViewcontroller.scrollsList objectAtIndex:_currentPage];
    UIImageView *curentDetailIV = (UIImageView *)[[currentDetailSV subviews] objectAtIndex:0];
    
    
    [UIView transitionWithView: self.view
                      duration: 0.70
                       options: UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        
                        curentDetailIV.frame = currentIVFrame;
                        [_reelViewcontroller.scrollView setBackgroundColor:[UIColor clearColor]];
                        
                    } completion:^(BOOL finished) {
                        [[[currentSV  subviews] objectAtIndex:0] setAlpha:1.0f];
                        [_reelViewcontroller.view removeFromSuperview];
                        _isShowingDetail = NO;
                        [self setNeedsStatusBarAppearanceUpdate];
                    }];
}

-(void)reelPageChanged:(int)page{
    //go to current page
    CGRect visibleFrame = _mainScrollView.frame;
    visibleFrame.origin.x = visibleFrame.size.width * page;
    visibleFrame.origin.y = 0;
    [_mainScrollView scrollRectToVisible:visibleFrame animated:NO];
    
    // hide current imageview
    UIScrollView *currentSV = (UIScrollView *)[_scrollsList objectAtIndex:_currentPage];
    for (UIScrollView *sv in _scrollsList) {
        if (currentSV != sv) {
            [[[sv  subviews] objectAtIndex:0] setAlpha:1.0f];
        }
        else{
            [[[sv  subviews] objectAtIndex:0] setAlpha:0.0f];
        }
    }
}

-(BOOL)prefersStatusBarHidden{
    if (_isShowingDetail) {
        return YES;
    }
    else{
        return NO;
    }
    
    return NO;
}

@end
