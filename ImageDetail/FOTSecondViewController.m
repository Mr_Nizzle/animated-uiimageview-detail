//
//  FOTSecondViewController.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/7/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTSecondViewController.h"
#import "FOTSingleImageDetailViewController.h"

@interface FOTSecondViewController () <SingleImageDetailDelegate>
@property (nonatomic, strong) FOTSingleImageDetailViewController *imageDetailView;
@property BOOL isShowingDetail;
@end

@implementation FOTSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Pan and Zoom";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(singleTapGestureCaptured:)];
    [_imageView addGestureRecognizer:singleTap];
    [_imageView setMultipleTouchEnabled:YES];
    [_imageView setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)singleTapGestureCaptured:(id)sender{
    _imageDetailView = [[FOTSingleImageDetailViewController alloc] initWithNibName:@"FOTSingleImageDetailViewController" bundle:[NSBundle mainBundle]];
    _imageDetailView.delegate = self;
    _imageDetailView.theImage = _imageView.image;
    _imageDetailView.originalFrame = [self.view convertRect: _imageView.frame fromView: _imageView.superview];
    [_imageDetailView.view setBackgroundColor:[UIColor clearColor]];
    [_imageDetailView.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.tabBarController.view addSubview:_imageDetailView.view];
    
    _imageDetailView.imageView = [[UIImageView alloc] initWithImage:_imageDetailView.theImage];
    _imageDetailView.imageView.frame = _imageDetailView.originalFrame;
    _imageDetailView.imageView.contentMode = _imageView.contentMode;
    _imageDetailView.imageView.userInteractionEnabled = YES;
    _imageDetailView.imageView.clipsToBounds = YES;
    _imageDetailView.imageView.backgroundColor = [UIColor purpleColor];
    
    [_imageDetailView.scrollView addSubview:_imageDetailView.imageView];
    
    [_imageView setHidden:YES];
    
    
    _imageDetailView.imageView.center = _imageView.center;
    
    _imageDetailView.imageView.transform = CGAffineTransformIdentity;
    _isShowingDetail = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [UIView transitionWithView: self.view
                      duration: 0.75
                       options: UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        
                        _imageDetailView.imageView.transform = CGAffineTransformMakeScale(2.0f,
                                                                                   2.0f);
                        _imageDetailView.imageView.center = _imageDetailView.scrollView.center;
                        
                        [_imageDetailView.view setBackgroundColor:[UIColor purpleColor]];
                        
                    } completion:^(BOOL finished) {
                        // do nothing
                    }];
}

-(void)shouldDismissImageDetail{
    [UIView transitionWithView: self.view
                      duration: 0.75
                       options: UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        
                        [_imageDetailView.view setBackgroundColor:[UIColor clearColor]];
                        
                        _imageDetailView.imageView.frame = _imageDetailView.originalFrame;
                        
                    } completion:^(BOOL finished) {
                        [_imageDetailView.view removeFromSuperview];
                        [_imageView setHidden:NO];
                        _isShowingDetail = NO;
                        [self setNeedsStatusBarAppearanceUpdate];
                    }];
}

-(BOOL)prefersStatusBarHidden{
    if (_isShowingDetail) {
        return YES;
    }
    else{
        return NO;
    }
    
    return NO;
}

@end
