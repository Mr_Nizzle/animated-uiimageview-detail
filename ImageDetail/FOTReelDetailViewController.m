//
//  FOTReelDetailViewController.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTReelDetailViewController.h"

@interface FOTReelDetailViewController () <UIScrollViewDelegate>
@property BOOL showingConectMenu;
@property int currentPage;

@end

@implementation FOTReelDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentContextMenu:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [_scrollView addGestureRecognizer:tapRecognizer];
    
    _topMenuView.alpha = 0.0f;
    _showingConectMenu = NO;
    
    _scrollView.delegate = self;
    
    _scrollsList = [[NSMutableArray alloc] initWithCapacity:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createPages{
    for (int i = 0; i < _imagesArr.count; i++) {
        // calc frame
        CGRect frame;
        frame.origin.x = (self.scrollView.frame.size.width - 0) * i;
        frame.origin.y = 0;
        frame.size = self.scrollView.frame.size;
        
        // create subview
        UIScrollView *subview = [[UIScrollView alloc] initWithFrame:frame];
        subview.tag = i;
        subview.minimumZoomScale = 1.0f;
        subview.maximumZoomScale = 3.0f;
        subview.delegate = self;
        [subview setScrollEnabled:YES];
        [subview setBackgroundColor:[UIColor clearColor]];
        
        // subview image
        UIImage *theImage = [UIImage imageNamed:[_imagesArr objectAtIndex:i]];
        
        float imgFactor = theImage.size.height / theImage.size.width;
        float imgWidth = [[UIScreen mainScreen] bounds].size.width;
        float imgHeight = frame.size.width * imgFactor;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 200, imgWidth, imgHeight)];
        subview.tag = i;
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
        [imageView setBackgroundColor:[UIColor blackColor]];
        [imageView setImage:theImage];
        
//        imageView.center = subview.center;
        
        [subview addSubview:imageView];
        
//        [subview setZoomScale:0.2f];
        
        //add subview
        [_scrollsList addObject:subview];
        [self.scrollView addSubview:subview];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * _imagesArr.count, self.scrollView.frame.size.height);
    
    //go to current page
    CGRect visibleFrame = _scrollView.frame;
    visibleFrame.origin.x = visibleFrame.size.width * _initPage;
    visibleFrame.origin.y = 0;
    [_scrollView scrollRectToVisible:visibleFrame animated:NO];
    
}

- (IBAction)dismissDetailView:(id)sender {
    UIScrollView *currentSV = (UIScrollView *)[_scrollsList objectAtIndex:_currentPage];
    [self hideMenu];
    
    [UIView transitionWithView: self.view
                      duration: 0.5
                       options: UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        
                        [currentSV setZoomScale:1.0f];
                        
                    } completion:^(BOOL finished) {
                        [[self delegate] shouldDismissReelDetail];
                    }];
}

#pragma mark -
#pragma mark menu methods

- (void)presentContextMenu:(id)sender{
    if (!_showingConectMenu) {
        
        [self showMenu];
        
    }else{
        [self hideMenu];
    }
}

-(void)showMenu{
//    UIScrollView *currentSV = (UIScrollView *)[_scrollsList objectAtIndex:_currentPage];
//    [currentSV setZoomScale:1.0f animated:YES];
    _topMenuView.alpha = 0.0f;
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration:0.3f];
    _topMenuView.alpha = 0.7f;
    [UIView commitAnimations];
    
    _showingConectMenu = YES;
}

-(void)hideMenu{
    _topMenuView.alpha = 0.7f;
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration:0.3f];
    _topMenuView.alpha = 0.0f;
    [UIView commitAnimations];
    
    _showingConectMenu = NO;
}

#pragma mark -
#pragma mark ScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    _currentPage = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    [self.delegate reelPageChanged:_currentPage];
    
    
    // hide current imageview
    UIScrollView *currentSV = (UIScrollView *)[_scrollsList objectAtIndex:_currentPage];
    for (UIScrollView *sv in _scrollsList) {
        if (currentSV != sv) {
            [sv setZoomScale:1.0f animated:YES];
        }
    }
    
//    [self hideMenu];
}


#pragma mark - UIScrollView Delegate
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //NSLog(@"subviews %@", [scrollView subviews]);
	return [[scrollView subviews] objectAtIndex:0];
}

- (void)centerImage{
    //_imageView.center = _scrollView.center;
}

//- (void)view:(UIView*)view setCenter:(CGPoint)centerPoint
//{
// TODO: fix this method.
//    CGRect vf = view.frame;
//    CGPoint co = _scrollView.contentOffset;
//    
//    CGFloat x = centerPoint.x - vf.size.width / 2.0;
//    CGFloat y = centerPoint.y - vf.size.height / 2.0;
//    
//    if(x < 0)
//    {
//        co.x = -x;
//        vf.origin.x = 0.0;
//    }
//    else
//    {
//        vf.origin.x = x;
//    }
//    if(y < 0)
//    {
//        co.y = -y;
//        vf.origin.y = 0.0;
//    }
//    else
//    {
//        vf.origin.y = y;
//    }
//    
//    view.frame = vf;
//    _scrollView.contentOffset = co;
//}

- (void)scrollViewDidZoom:(UIScrollView *)sv
{
    if (sv != _scrollView) {
//        [self hideMenu];
        UIView* zoomView = [sv.delegate viewForZoomingInScrollView:sv];
        CGRect zvf = zoomView.frame;
        if(zvf.size.width < sv.bounds.size.width)
        {
            zvf.origin.x = (sv.bounds.size.width - zvf.size.width) / 2.0;
        }
        else
        {
            zvf.origin.x = 0.0;
        }
        if(zvf.size.height < sv.bounds.size.height)
        {
            zvf.origin.y = (sv.bounds.size.height - zvf.size.height) / 2.0;
        }
        else
        {
            zvf.origin.y = 0.0;
        }
        zoomView.frame = zvf;
    }
}

@end
