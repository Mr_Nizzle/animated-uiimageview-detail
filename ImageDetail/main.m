//
//  main.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/7/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FOTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FOTAppDelegate class]));
    }
}
