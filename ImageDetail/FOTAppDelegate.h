//
//  FOTAppDelegate.h
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/7/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FOTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
