//
//  FOTSingleImageDetailViewController.m
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/8/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTSingleImageDetailViewController.h"

@interface FOTSingleImageDetailViewController () <UIScrollViewDelegate>
@property BOOL showingConectMenu;
@end

@implementation FOTSingleImageDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _scrollView.minimumZoomScale = 1.0f;
    _scrollView.maximumZoomScale = 3.0f;
    _scrollView.delegate = self;
    [_scrollView setScrollEnabled:YES];
    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [_scrollView addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentContextMenu:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [_scrollView addGestureRecognizer:tapRecognizer];
    
    _topMenuView.alpha = 0.0f;
    _showingConectMenu = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissImageDetail:(id)sender {
    
    [self hideMenu];
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration:0.2f];
    [_scrollView setZoomScale:1.0];
    [self centerImage];
    [UIView commitAnimations];
    
    [[self delegate] shouldDismissImageDetail];
}


#pragma mark - UIScrollView Delegate
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return _imageView;
}

- (void)centerImage{
    _imageView.center = _scrollView.center;
}

- (void)view:(UIView*)view setCenter:(CGPoint)centerPoint
{
    CGRect vf = view.frame;
    CGPoint co = _scrollView.contentOffset;
    
    CGFloat x = centerPoint.x - vf.size.width / 2.0;
    CGFloat y = centerPoint.y - vf.size.height / 2.0;
    
    if(x < 0)
    {
        co.x = -x;
        vf.origin.x = 0.0;
    }
    else
    {
        vf.origin.x = x;
    }
    if(y < 0)
    {
        co.y = -y;
        vf.origin.y = 0.0;
    }
    else
    {
        vf.origin.y = y;
    }
    
    view.frame = vf;
    _scrollView.contentOffset = co;
}

- (void)scrollViewDidZoom:(UIScrollView *)sv
{
    UIView* zoomView = [sv.delegate viewForZoomingInScrollView:sv];
    CGRect zvf = zoomView.frame;
    if(zvf.size.width < sv.bounds.size.width)
    {
        zvf.origin.x = (sv.bounds.size.width - zvf.size.width) / 2.0;
    }
    else
    {
        zvf.origin.x = 0.0;
    }
    if(zvf.size.height < sv.bounds.size.height)
    {
        zvf.origin.y = (sv.bounds.size.height - zvf.size.height) / 2.0;
    }
    else
    {
        zvf.origin.y = 0.0;
    }
    zoomView.frame = zvf;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    // 1
    CGPoint pointInView = [recognizer locationInView:_imageView];
    
    // 2
    CGFloat newZoomScale = _scrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, _scrollView.maximumZoomScale);
    
    // 3
    CGSize scrollViewSize = _scrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    // 4
    [_scrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)presentContextMenu:(id)sender{
    if (!_showingConectMenu) {
        
        [self showMenu];
        
    }else{
        [self hideMenu];
    }
}

-(void)showMenu{
    _topMenuView.alpha = 0.0f;
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration:0.3f];
    _topMenuView.alpha = 0.7f;
    [UIView commitAnimations];
    
    _showingConectMenu = YES;
}

-(void)hideMenu{
    _topMenuView.alpha = 0.7f;
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration:0.3f];
    _topMenuView.alpha = 0.0f;
    [UIView commitAnimations];
    
    _showingConectMenu = NO;
}

@end
