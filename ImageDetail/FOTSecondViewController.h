//
//  FOTSecondViewController.h
//  ImageDetail
//
//  Created by Ricardo Guillen on 10/7/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FOTSecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
